package com.example.slidelinedemo;

import android.webkit.JavascriptInterface;

import com.google.gson.Gson;

import java.util.Map;

public class JsDataUtils {
    private Map<String, String> map;
    private Gson mGson = new Gson();

    /**
     * 通过构造函数像js传递网络请求的参数,以map作为容器，后面再转成字符串
     *
     * @param map
     */
    public JsDataUtils(Map<String, String> map) {
        this.map = map;
    }

    @JavascriptInterface
    public String stringToHtml() {
        return mGson.toJson(map);
    }
}
